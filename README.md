# (DEPRECATED) Simple EC Policies

**This repo is now deprecated. The policy rules defined here are now part of the
[slsa_source_correlated](https://enterprisecontract.dev/docs/ec-policies/release_policy.html#slsa_source_correlated_package)
policy package. Use that instead.**

This repository hosts Enterprise Contract (EC) policies meant to assist in validating the provenance
of container images. It is a complement to the
[image-provenance-poc](https://gitlab.com/lucarval/image-provenance-poc) repository.

## Usage

Create a policy config, called `policy.yaml`, to use the policies from this repository:

```yaml
sources:
  - policy:
    - git::https://gitlab.com/lucarval/simple-ec-policies
```

One of the checks provided by the policies in this repository is to verify the image was built from
the expected git repository. In order to do this, the expected git repository must be provided to
the EC CLI. Create an `images.yaml` file to capture this information. Below is an example to help
you get started. Replace values accordingly.

```yaml
components:
  - containerImage: localhost:5000/image-provenance-poc:latest
    source:
      git:
        url: https://gitlab.com/lucarval/image-provenance-poc
        revision: 955f845864dae559ee23d79b49c492fea621eda7
```

Finally, validate the image with the EC CLI:

```text
ec validate image --public-key cosign.pub --images images.json --policy policy.yaml
```

NOTE: The `--images` parameter is used instead of the `--image` parameter. This is so we can provide
additional information about the image being verified.

You can use the `--output yaml` parameter to change the report to a more readable format, YAML.
Also, consider using the `--show-successes` parameters to view all the checks performed.
