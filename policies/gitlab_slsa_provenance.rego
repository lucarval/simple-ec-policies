# METADATA
# title: GitLab SLSA Provenance
# description: >-
#   Verify the SLSA Provenance created from GitLab meets requirements.
package policies.gitlab_slsa_provenance

import rego.v1

# METADATA
# title: Resolved Dependencies
# description: Verify the resolved dependencies on the SLSA Provenance are as expected.
# custom:
#   short_name: resolved_dependencies
#
deny contains result if {
	some error in _errors
	result := {
		"msg": error,
		"metadata": {"code": "gitlab_slsa_provenance.resolved_dependencies"},
	}
}

_errors contains error if {
	not input.image.source
	error := "Input does not contain source information"
}

_errors contains error if {
	input.image.source == {}
	error := "Input does not contain an expectation for git source"
}

_errors contains error if {
	count(_slsa_provenance_attestations) == 0
	error := "No v1 SLSA Provenance statements found"
}

_errors contains error if {
	some att in _slsa_provenance_attestations
	count(_resolved_dependencies(att)) == 0
	error := "SLSA Provenance does not contain any resolved dependencies"
}

_errors contains error if {
	some att in _slsa_provenance_attestations

	matches := [dep |
		some dep in _resolved_dependencies(att)
		dep.uri == _expected_source.uri
		some _, digest in dep.digest
		_expected_source.revision == digest
	]

	count(matches) == 0

	error := sprintf(
		"Cannot find a match for expected git source with uri %q and revision %q",
		[_expected_source.uri, _expected_source.revision],
	)
}

_slsa_provenance_attestations := [att |
	some att in input.attestations
	att.statement.predicateType == "https://slsa.dev/provenance/v1"
]

_resolved_dependencies(att) := deps if {
	deps := [dep |
		some dep in att.statement.predicate.buildDefinition.resolvedDependencies
	]
} else := []

_expected_source := {
	"uri": input.image.source.git.url,
	"revision": input.image.source.git.revision,
}
